package com.customerfeign.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.customerfeign.model.OrderRequest;

//@FeignClient(value="order-service", url="http://localhost:8082/test/orders")
@FeignClient(name="http://ORDER-SERVICE/test/orders")
public interface CustomerFeign {

	@GetMapping("")
	public List<OrderRequest> getAll();
	
	@PostMapping("/requestBody")
	public OrderRequest saveOrderData(@RequestBody OrderRequest order);
	
	@PutMapping("/{ordId}")
	public OrderRequest updateOrderData(@PathVariable Integer ordId, @RequestBody OrderRequest order);
	
	@DeleteMapping("/{ordId}")
	public ResponseEntity<String> deleteOrder(@PathVariable Integer ordId);

}
