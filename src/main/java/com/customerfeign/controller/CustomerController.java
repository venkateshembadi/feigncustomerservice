package com.customerfeign.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customerfeign.feign.CustomerFeign;
import com.customerfeign.model.OrderRequest;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	@Autowired
	public CustomerFeign feign;

	@PostMapping("/customer")
	public OrderRequest saveCustomerOrder(@RequestBody OrderRequest order) {
		OrderRequest response = feign.saveOrderData(order);
		return response;

	}

	@PutMapping("/customer/{ordId}")
	public OrderRequest updateCustomerOrder(@PathVariable Integer ordId, @RequestBody OrderRequest order) {
		OrderRequest response = feign.updateOrderData(ordId, order);
		return response;

	}

	@DeleteMapping("/{ordId}")
	public ResponseEntity<String> deleteOrder(@PathVariable Integer ordId) {
		ResponseEntity<String> response=feign.deleteOrder(ordId);
		return response;
	}

	@GetMapping
	public List<OrderRequest> getCustomerOrders() {
		return feign.getAll();

	}
}
