package com.customerfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CustomerfeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerfeignApplication.class, args);
	}

}
